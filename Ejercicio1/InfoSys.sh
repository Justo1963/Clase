
function numCpus {
	cat /proc/cpuinfo | grep processor | wc -l > CPUS.txt
}

function usbPci {
	echo "bus USB"
	echo "======="
	lsusb
	echo 
	echo
	echo "bus PCI"
	echo "======="
	lspci 
}

function usoDeDiscos {	
	df -h > discos.txt	
}


function usoDeRam {
	free -h > memoria.txt
}

function numeroDeUsuarios {
	who | wc -l > numusu.txt 
	
}

function Usuarios {
	who  > usuarios.txt 
	
}

function servicios {
	service --status-all | grep + > servicios.txt 
}




numCpus
usbPci
usoDeDiscos	
usoDeRam 
numeroDeUsuarios
Usuarios
servicios

